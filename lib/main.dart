import 'dart:math';

import 'package:flutter/material.dart';
var RowCount = 20;
var ColumnCount = 20;
var color_list = [];
var cart_color_list = [];
void main() {
  for (var i = 0; i < RowCount; i++){
    color_list.add(<MaterialColor>[]);
    for (var j = 0; j < ColumnCount; j++)
    {
      var color = Colors.primaries[Random().nextInt(Colors.primaries.length)];
      color_list[i].add( color == Colors.grey?Colors.primaries[Random().nextInt(Colors.primaries.length)]:color);
    }
  }
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
   MyApp({Key? key}) : super(key: key);
  static  String _title = 'Flutter Code Sample';
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return  MaterialApp(
      title: _title,
      initialRoute: '/futureBuilder',
      routes: {
        '/futureBuilder': (context) => MyStatefulWidget(title:_title),
        '/homepage':(context) => MyHomePage(title:_title),
        '/rowsAndColumns':(context) => RowsAndColumns(title:_title),
      },
      home: MyStatefulWidget(title:_title),
    );
  }
}

class RowsAndColumns extends StatefulWidget {
  RowsAndColumns({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<RowsAndColumns> createState() => _RowsAndColumnsState();
}
class _RowsAndColumnsState extends State<RowsAndColumns> with TickerProviderStateMixin{
  var selected_index = new Map();
  var selected_cart_index;
  var final_position = new Map();
  var initial_position = new Map();
  bool isNotFromCart = false;
  late AnimationController _controller;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(seconds: 1),
      vsync: this,
    )
      ..repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      title: Text(widget.title),
    );
    double size = MediaQuery.of(context).size.width/4;
    var height = size;
    var width = size;
    var default_container = Container(
      color: Colors.grey,
      child: ColorFiltered(
        colorFilter: ColorFilter.mode(
          Colors.grey,
          BlendMode.saturation,
        ),
        child:FlutterLogo(
        size: size,
      ),
    ),
    );

    List<Widget> rows = <Widget>[];
    for (var i = 0; i < RowCount; i++) {
      List<Widget> rowItemList = <Widget>[];
      for (var j = 0; j < ColumnCount; j++) {
        var container = Container(
          color: color_list[i][j],
          child: FlutterLogo(
            size: size,
          ),
        );

        var feedback_container = AnimatedBuilder(
          animation: _controller,
          child:Container(
          color: color_list[i][j],
          child: FlutterLogo(
            size: size+(0.25*size),
          ),
        ),builder: (BuildContext context, Widget? child) {
          return Transform.rotate(
            angle: _controller.value * pi/8,
            child: Transform.rotate(angle:-pi/16 ,child:child),
          );
        },
    );
        rowItemList.add(
            LongPressDraggable<MaterialColor>(
              data:color_list[i][j],
              child:container,
              feedback: feedback_container,
              childWhenDragging: default_container,
              onDragStarted: ()
              {
                isNotFromCart = true;
                selected_index = {i:j};
              },
              onDragUpdate:(dragDetails){
                if(initial_position.isEmpty)
                  initial_position ={dragDetails.globalPosition.dx:(dragDetails.globalPosition.dy-appBar.preferredSize.height-MediaQuery.of(context).padding.top)};
              },
              onDragEnd: (dragDetails)
              {
                isNotFromCart = false;
                final_position ={dragDetails.offset.dx:(dragDetails.offset.dy-appBar.preferredSize.height-MediaQuery.of(context).padding.top)};
              },
        )
        );
      }
      rows.add(
            LimitedBox(
              maxHeight:height,
              child:ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  addAutomaticKeepAlives: false,
                  itemCount: rowItemList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return DragTarget<MaterialColor>(
                      builder: (context,candidateData,rejectedData) {
                        return rowItemList[index];
                      },
                      onWillAccept: (data) {
                        return true;
                      },
                      onAccept: (data) {
                        if (selected_index.keys.isNotEmpty) {
                          var temp = color_list[i][index];
                          color_list[i][index] = data;
                          color_list[selected_index.keys.first][selected_index[selected_index.keys.first]] = temp;

                          setState((){
                            Transform.translate(offset: Offset(initial_position.keys.first,initial_position[initial_position.keys.first]),child: rowItemList[index]);
                            if(initial_position.isNotEmpty)
                              initial_position.clear();
                          });
                        }
                      },
                      onMove: (dragDetails){

                      },
                    );
                  }
              ),
            ),
      );
    }
    List<Widget> cart = <Widget>[];
    if( cart_color_list.isNotEmpty){
      cart_color_list.forEach((element) => cart.add(
          LongPressDraggable<MaterialColor>(
              data:element,
              child:Container(
                color: element,
                child: FlutterLogo(
                  size: size,
                ),
              ),
              feedback: AnimatedBuilder(
                animation: _controller,
                child:Container(
                  color: element,
                  child: FlutterLogo(
                    size: size+(0.25*size),
                  ),
                ),builder: (BuildContext context, Widget? child) {
                return Transform.rotate(
                  angle: _controller.value * pi/8,
                  child: Transform.rotate(angle:-pi/16 ,child:child),
                );
              },
              ),
              childWhenDragging: default_container,
            onDragStarted: (){
                selected_cart_index = cart_color_list.indexOf(element);
            },
          )
        )
      );
      }
    else
      cart.add(Container(
        color: Colors.white,
        child: ColorFiltered(
          colorFilter: ColorFilter.mode(
            Colors.white,
            BlendMode.saturation,
          ),
          child:FlutterLogo(
            size: MediaQuery.of(context).size.width,
          ),
        ),
      ));
    return Scaffold(
      appBar: appBar,
      body:Column(children: [
      LimitedBox(
        maxHeight:MediaQuery.of(context).size.height-appBar.preferredSize.height-MediaQuery.of(context).padding.top-size,
        maxWidth:width,
        child:ListView.builder(
            scrollDirection: Axis.vertical,
            addAutomaticKeepAlives: false,
            itemCount: rows.length,
            itemBuilder: (BuildContext context, int index) {
              return rows[index];
            }
        ),
      ),
    DragTarget<MaterialColor>(
    builder: (context,candidateData,rejectedData) {
      ScrollController _scrollController = new ScrollController();
    return Row(
          children: [
          LimitedBox(
            maxHeight:size,
            maxWidth:MediaQuery.of(context).size.width,
            child: ListView.builder(
              padding: EdgeInsets.only(right: size),
                scrollDirection: Axis.horizontal,
                controller: _scrollController,
                addAutomaticKeepAlives: false,
                itemCount: cart.length,
                itemBuilder: (BuildContext context, int index) {
                  return DragTarget<MaterialColor>(
                      builder: (context,candidateData,rejectedData) {
                        return cart[index];
                      },
                      onWillAccept: (data) {
                        return true;
                      },
                      onAccept: (data) {
                        if( cart_color_list.isNotEmpty)
                          if(isNotFromCart)
                            cart_color_list.insert(index, data);
                          else
                            {
                            var temp = cart_color_list[index];
                            cart_color_list[index] = data;
                            cart_color_list[selected_cart_index] = temp;
                          }
                        else
                          cart_color_list.add(data);
                        setState((){});
                      },
                    onMove: (dragDetails){
                        if(index == cart.indexOf(cart.last) && cart.length>1)
                          {
                            _scrollController.animateTo(_scrollController.position.maxScrollExtent,
                                curve: Curves.easeOut,
                                duration: Duration(milliseconds: 300));
                          }
                    },
                  );
                }
            ),
          ),
        ],
        );
    },
        onWillAccept: (data) {
          return true;
        },
        onAccept: (data) {
            cart_color_list.add(data);
          setState((){});
        }
    ),

    ],
    ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
   MyStatefulWidget({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final Future<String> _calculation = Future<String>.delayed(
    const Duration(seconds: 2),
        () => 'Data Loaded',
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
      body: Center(
    // Center is a layout widget. It takes a single child and positions it
    // in the middle of the parent.
    child: DefaultTextStyle(
      style: Theme.of(context).textTheme.headline2!,
      textAlign: TextAlign.center,
      child: FutureBuilder<String>(
        future: _calculation, // a previously-obtained Future<String> or null
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          List<Widget> children;
          if (snapshot.hasData) {
            children = <Widget>[
              const Icon(
                Icons.check_circle_outline,
                color: Colors.green,
                size: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Result: ${snapshot.data}'),
              )
            ];
          } else if (snapshot.hasError) {
            children = <Widget>[
              const Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Error: ${snapshot.error}'),
              )
            ];
          } else {
            children = const <Widget>[
              SizedBox(
                child: CircularProgressIndicator(),
                width: 60,
                height: 60,
              ),
              Padding(
                padding: EdgeInsets.only(top: 16),
                child: Text('Awaiting result...'),
              )
            ];
          }
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: children,
            ),
          );
        },
      ),
    ),
    ),
      floatingActionButton: FloatingActionButton(
        onPressed: ()async {Navigator.popAndPushNamed(context, '/homepage');},
        tooltip: 'Increment',
        child: Icon(Icons.navigate_next),
      ),
    );
  }
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      Align(
        alignment: Alignment.bottomRight,
        child:FloatingActionButton(
          onPressed: _incrementCounter,
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
      Align(
        alignment: Alignment.bottomRight,
        child:FloatingActionButton(
          onPressed: ()async{Navigator.popAndPushNamed(context, '/rowsAndColumns');},
          tooltip: 'Next',
          child: Icon(Icons.navigate_next),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
        ],
      ),
    );
  }
}
